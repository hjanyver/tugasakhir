module.exports = {
  content: [
    './src/**/*.{js,jsx,ts,tsx}',
    'node_modules/flowbite-react/**/*.{js,jsx,ts,tsx}'],
  theme: {
    screens: {
      sm: '480px',
      md: '768px',
      lg: '976px',
      xl: '1200px',
    },
    fontSize: {
      sm: ['12px', '20px'],
      base: ['16px', '24px'],
      lg: ['20px', '28px'],
      xl: ['24px', '32px'],
      normal: ['14px', '20px']
    },

    fontFamily: {
      sans: ['Arial', 'sans-serif'],
      serif: ['Arial', 'serif'],
    },
    extend: {
      spacing: {
        '128': '32rem',
        '144': '36rem',
      },
      colors: {
        'white': '#ffffff',
        'blue': '#1fb6ff',
        'red': '#bf0711',
        'gray-dark': '#273444',
        'gray': '#8492a6',
        'gray-light': '#f5f5f5',
      },
      borderRadius: {
        '4xl': '2rem',
      }
    }
  },
}

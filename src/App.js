import React, { useEffect, useContext } from "react";
import { createBrowserRouter, RouterProvider } from 'react-router-dom';

import Home from "./pages/Home";
import Login from "./components/Login";
import Register from "./components/Register";
import Products from "./pages/Products";
import Setting from "./pages/Setting";
import Editproducts from "./pages/Editproduct";
import Detail from "./pages/Detail";
import Create from "./pages/Create";

const router = createBrowserRouter([
  {
    path: '/',
    element: <Home />,
  },
  {
    path: '/login',
    element: <Login />,
  },
  {
    path: '/register',
    element: <Register />,
  },
  {
    path: '/products',
    element: <Products />,
  },
  {
    path: '/setting',
    element: <Setting />,
  },
  
  {
    path: '/create',
    element: <Create />,
  },
  
  {
    path: '/setting/edit/:id',
    element: <Editproducts />,
  },
  {
    path: '/product/:id',
    element: <Detail />,
  }
]);
function App() {

  return (
    <div>
      <RouterProvider router={router} />
    </div>
  );
}
export default App;
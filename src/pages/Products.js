import React from 'react';
import Header from './Header';
import Allproduct from '../components/Allproduct';

function Products() {
    return (
        <div className='pb-[100px]'>
            <Header />
            <Allproduct />
        </div>
    )
}
export default Products;
import React from 'react';
import Header from './Header';
import Productdetail from '../components/Productdetail';

function Detail() {
    return (
        <div className='pb-[100px]'>
            <Header />
            <Productdetail />
        </div>
    )
}
export default Detail;
import React from 'react';
import Header from './Header';
import TableAllProduct from '../components/TableAllProduct';

function Setting() {
    return (
        <div className='pb-[100px]'>
            <Header />
            <TableAllProduct />
        </div>
    )
}
export default Setting;
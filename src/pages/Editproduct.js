import React from 'react';
import Header from './Header';
import Editproduct from '../components/Editproduct';

function Editproducts() {
    return (
        <div className='pb-[100px]'>
            <Header />
            <Editproduct />
        </div>
    )
}
export default Editproducts;
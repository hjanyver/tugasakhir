import React from 'react';
import Header from './Header';
import Mainproduct from '../components/Mainproduct';

function Home() {
    return (
        <div className='pb-[100px]'>
            <Header />
            <Mainproduct />
        </div>
    )
}
export default Home;
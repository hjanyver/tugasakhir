import React from 'react';
import Header from './Header';
import Addproduct from '../components/Addproduct';

function Create() {
    return (
        <div className='pb-[100px]'>
            <Header />
            <Addproduct />
        </div>
    )
}
export default Create;
import axios from "axios";
import React, { useContext } from "react";
import { ProdContext } from "../GlobalContext";
import { Link, useNavigate } from "react-router-dom";
import logo from '../logo.png';
import { Dropdown } from 'flowbite-react';


function Header() {
    const { setShowMessage } = useContext(ProdContext);
    const getuser = localStorage.getItem('username');
    const token = localStorage.getItem('token');
    const username = 'Hello, ' + getuser;
    const navigate = useNavigate();

    const logout = async () => {
        try {
            const response = await axios.post("https://arhandev.xyz/public/api/final/logout",
                {},
                { headers: { Authorization: `Bearer ${token}` } }
            );
            localStorage.removeItem('token');
            localStorage.removeItem('username');
            navigate("/");
            setShowMessage(false);
        } catch (error) {
            console.log(error);
        }
    };

    return (
        <div className="header w-full fixed z-20">
            <div className=" bg-white w-full">
                <div className="container mx-auto py-3">
                    <nav className="flex justify-around md:justify-between content-center items-center flex-wrap md:flexnowrap">
                        <ul className="flex space-x-8 ml-5 order-2 ">
                            <li>
                                <Link to={'/'}>Home</Link>
                            </li>
                            <li>
                                <Link to={'/products'}>Products</Link>
                            </li>
                        </ul>
                        <div className="flex items-center order-1">
                            <img src={logo} alt="Logo" style={{maxHeight:'50px'}}/>
                        </div>
                        <div className="flex space-x-8 md:order-3">
                            {getuser !== null ? (
                                <div className="text-normal text-blue-500 hover:bg-blue-500  border-blue-500 rounded bg-blue ">
                                    <Dropdown className="border-0 focus:ring-0 text-normal text-blue-500 "
                                        label ={username}
                                        dismissOnClick={false}
                                    >
                                        <Dropdown.Item className="block py-2 px-6 hover:bg-blue ">
                                            <Link to={"/setting"}> <span className="flex gap-1 items-center">Settings</span></Link>
                                        </Dropdown.Item>
                                        <Dropdown.Item className="block py-2 px-6 hover:bg-blue">
                                            <span className="flex gap-1 items-center" onClick={logout} >Logout</span>
                                        </Dropdown.Item>

                                    </Dropdown>
                                </div>
                            ) : (
                                <div className="flex gap-4">
                                    <button className=""><Link to={'/login'}>Login</Link></button>
                                    <Link to={'/register'}><button className="drop-shadow-xl bg-blue text-white px-4 py-2 flex items-center ">Register</button></Link>
                                </div>
                            )}
                        </div>
                    </nav>
                </div>
            </div >
        </div >
    )
}

export default Header;
import axios from "axios";
import React, { useEffect, useState, useContext } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { CiCircleCheck } from "react-icons/ci";
import { ProdContext } from "../GlobalContext";
import { Formik } from "formik";
import * as Yup from 'yup';

const Editproduct = () => {

    const navigate = useNavigate();
    const { id } = useParams();
    const validationScheme = Yup.object().shape({
        nama: Yup.string().required("required"),
        harga: Yup.number().min(100, 'Price less than 100').typeError('must be a number').required("required"),
        is_diskon: Yup.boolean(),
        harga_diskon: Yup.number().when('is_diskon', {
            is: true,
            then: Yup.number().typeError('must be a number').required('required'),
            otherwise: Yup.number()
        }),
        stock: Yup.number().min(1, 'Minumum stock is 1').required("required"),
        description: Yup.string().required("required"),
        category: Yup.string().required("required"),
        image_url: Yup.string().required("required").url()
    });

    const [ShowDiscount, setShowDiscount] = useState(false);
    const { getProductDetail, Einput, EsetInput, input, setInput, ShowMessage, setShowMessage, Poperror, setPoperror, setmsg, setShowMessageSuccess, ShowMessageSuccess, isdiscount, setIsdiscount } = useContext(ProdContext);
    const HandleDiscount = (e) => {
        setShowDiscount(!ShowDiscount);
        setIsdiscount((current) => !current);
    };

    const handleSubmit = async (values) => {
        try {
            const token = localStorage.getItem('token');
            const response = await axios.put(
                `https://arhandev.xyz/public/api/products/${id}`,
                {
                    nama: values.nama,
                    harga: values.harga,
                    harga_diskon: values.harga_diskon,
                    is_diskon: isdiscount,
                    stock: values.stock,
                    category: values.category,
                    image_url: values.image_url,
                    description: values.description
                },
                { headers: { Authorization: `Bearer ${token}` } }
            );

            //show message success
            setmsg(response.data.info);
            setShowMessageSuccess(true);

            setIsdiscount(false);
            setInput({
                nama: "",
                harga: "",
                harga_diskon: "",
                is_diskon: "",
                stock: "",
                category: "",
                image_url: "",
                description: ""
            });

            navigate('/setting');
        } catch (error) {
            //show error
            console.log(error);
            setmsg(error.response.data.info);
            setShowMessageSuccess(true);
        }
    };

    useEffect(() => {
        getProductDetail(id);
    }, []);
    return (
        <div className="bg-gray-light pt-[150px]">
            <div className="container mx-auto">

                <div className="bg-white mt-4 px-10 py-10 mx-auto shadow-lg sm:rounded-lg w-5/6">
                    <p className="mb-10"><b>Edit product</b></p>
                    <Formik enableReinitialize onSubmit={handleSubmit} initialValues={Einput} validationSchema={validationScheme}>
                        {params => {
                            return (
                                <form>
                                    <div className="mb-4">
                                        <label htmlFor="nama" className="block mb-2 text-normal">Product name</label>
                                        <input value={params.values.nama}
                                            onChange={params.handleChange}
                                            onBlur={params.handleBlur}
                                            type="text" id="nama" className="border border-gray text-grey text-sm focus:outline-none focus:border-1 focus:ring-1 w-full p-2 block " placeholder="Enter a product name" />
                                        {params.touched.nama && params.errors.nama && <label className="text-sm text-red">{params.errors.nama}</label>}
                                    </div>
                                    <div className="grid gap-6 mb-6 md:grid-cols-2 items-center">
                                        <div>
                                            <label htmlFor="harga" className="block mb-2 text-normal">Price</label>
                                            <div className="relative">
                                                <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                                    <span className="text-sm text-grey">Rp. </span>
                                                </div>
                                                <input value={params.values.harga}
                                                    onChange={params.handleChange}
                                                    onBlur={params.handleBlur}
                                                    name="harga" type="text" id="harga" className="pl-10 border border-gray text-grey text-sm focus:outline-none focus:border-1 focus:ring-1 w-full p-2 block" placeholder="Enter a price" />
                                            </div>
                                            {params.touched.harga && params.errors.harga && <label className="text-sm text-red">{params.errors.harga}</label>}

                                        </div>
                                        <div className="flex gap-5 justify-between content-center items-center mt-5">
                                            <div className="flex gap-2 items-center mb-2 ">
                                                <input type="checkbox" name="is_diskon"
                                                    checked={isdiscount}
                                                    value={isdiscount}
                                                    onChange={params.handleChange}
                                                    onBlur={params.handleBlur}
                                                    onClick={HandleDiscount} />
                                                <label htmlFor="is_diskon" className="block text-normal">Discount?</label>
                                            </div>
                                            <div className="w-full">

                                                {isdiscount === true && (
                                                    <div>
                                                        <div className="relative">
                                                            <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                                                <span className="text-sm text-grey">Rp. </span>
                                                            </div>
                                                            <input value={params.values.harga_diskon}
                                                                onChange={params.handleChange}
                                                                onBlur={params.handleBlur}
                                                                type="text" id="harga_diskon" name="harga_diskon" className="pl-10 border border-gray text-grey text-sm focus:outline-none focus:border-1 focus:ring-1 w-full p-2 block" placeholder="Enter a price" />

                                                        </div>
                                                        {params.touched.harga_diskon && params.errors.harga_diskon && <label className="text-sm text-red">{params.errors.harga_diskon}</label>}
                                                    </div>
                                                )}
                                            </div>
                                        </div>
                                    </div>
                                    <div className="grid gap-6 mb-6 md:grid-cols-2">
                                        <div>
                                            <label htmlFor="category" className="block mb-2 text-normal">Category</label>
                                            <select value={params.values.category}
                                                onChange={params.handleChange}
                                                onBlur={params.handleBlur}
                                                name="category" className="border border-gray text-grey text-sm focus:outline-none focus:border-1 focus:ring-1 w-full p-2 block">
                                                <option defaultValue>Choose a category</option>
                                                <option value="teknologi">Teknologi</option>
                                                <option value="makanan">Makanan</option>
                                                <option value="minuman">Minuman</option>
                                                <option value="lainnya">Lainnya</option>
                                            </select>
                                            {params.touched.category && params.errors.category && <label className="text-sm text-red">{params.errors.category}</label>}
                                        </div>
                                        <div>
                                            <label htmlFor="stock" className="block mb-2 text-normal">Stock</label>
                                            <input value={params.values.stock}
                                                onChange={params.handleChange}
                                                onBlur={params.handleBlur}
                                                type="text" id="stock" className="border border-gray text-grey text-sm focus:outline-none focus:border-1 focus:ring-1 w-full p-2 block" placeholder="Enter stock" />
                                            {params.touched.stock && params.errors.stock && <label className="text-sm text-red">{params.errors.stock}</label>}
                                        </div>

                                    </div>

                                    <div className="mb-6">
                                        <label htmlFor="image_url" className="block mb-2 text-normal">Product image</label>
                                        <input value={params.values.image_url}
                                            onChange={params.handleChange}
                                            onBlur={params.handleBlur}
                                            type="text" id="image_url" name="image_url" className="border border-gray text-grey text-sm  focus:outline-none focus:border-1 focus:ring-1 w-full p-2 block" placeholder="https://" />
                                        {params.touched.image_url && params.errors.image_url && <label className="text-sm text-red">{params.errors.image_url}</label>}
                                    </div>
                                    <div className="mb-6">
                                        <label htmlFor="description" className="block mb-2 text-normal">Product description</label>
                                        <textarea value={params.values.description}
                                            onChange={params.handleChange}
                                            onBlur={params.handleBlur}
                                            id="description" name="description" className="border border-gray text-grey text-sm focus:outline-none focus:border-1 focus:ring-1 w-full p-2 block" placeholder="Enter product description"></textarea>
                                        {params.touched.description && params.errors.description && <label className="text-sm text-red">{params.errors.description}</label>}
                                    </div>
                                    <div className="">
                                        <button onClick={params.handleSubmit}
                                            type="submit"
                                            name="submit" className={params.dirty && params.isValid ? 'text-normal drop-shadow-xl border border-blue text-white bg-blue px-4 py-2 flex items-center enabled' : 'text-normal border border-blue text-white bg-blue px-4 py-2 flex items-center opacity-75'} ><CiCircleCheck size="22px" className="pr-1" />Update</button>
                                    </div>
                                </form>
                            )
                        }}
                    </Formik>
                </div>
            </div>
        </div>
    );
};

export default Editproduct;
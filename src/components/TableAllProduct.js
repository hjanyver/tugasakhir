import axios from "axios";
import React, { useEffect, useState, useContext } from "react";
import { Link, useNavigate } from "react-router-dom";
import Skeleton from 'react-loading-skeleton'
import { CiCircleCheck } from "react-icons/ci";
import { Modal, Button, Alert } from 'flowbite-react';
import { ProdContext } from "../GlobalContext";

function TableAllProduct() {
    const [openModal, setOpenModal] = useState(false);
    const [deleteBtn, setDeleteBtn] = useState('');

    const navigate = useNavigate();
    const { getProduct, loading, Prods, filterHighlight, setFilterHighlight, search, setSearch, displayData, setDisplayData, setmsg, setShowMessageSuccess, ShowMessageSuccess, Popmsg } = useContext(ProdContext);


    const handleModalOpen = (id) => {
        setOpenModal(true);
        setDeleteBtn(id);
    }

    const deleteProduct = async (id) => {
        try {
            const token = localStorage.getItem('token');
            const response = await axios.delete(
                `https://arhandev.xyz/public/api/final/products/${id}`,
                { headers: { Authorization: `Bearer ${token}` } }
            );
            setOpenModal(false);
            setmsg(response.data.info);
            setShowMessageSuccess(true);
            getProduct();
            navigate('/setting');
        }
        catch (error) {
            console.log(error);
        }
    };

    const handleChange = event => {
        setSearch(event.target.value);
    };
    const onFilter = () => {
        let arrFilter = [...Prods];
        if (filterHighlight !== '') {
            arrFilter = arrFilter.filter(prod => prod.category == filterHighlight.toLowerCase());
        }

        if (search !== '') {
            arrFilter = arrFilter.filter(prod => prod.nama.toLowerCase().includes(search.toLowerCase()));
        }
        setDisplayData(arrFilter);
    };

    const onReset = () => {
        setDisplayData(Prods);
        setSearch('');
        setFilterHighlight('');
    };

    useEffect(() => {
        getProduct();
    }, []);

    return (

        <div className="bg-gray-light pt-[150px]">
            <div className="container mx-auto">

                <div className="bg-white mb-4 mt-4 px-10 py-5 flex justify-between content-center items-center">
                    <p className=""><b>Setting</b></p>
                    <Link to={"/create"}><button className="text-sm border-blue text-white bg-blue px-4 py-2 flex items-center hover:bg-sky-600 "> Add product</button></Link>
                </div>
                <div className="pt-30">
                    {ShowMessageSuccess === true && (
                        <Alert className="bg-light-green mb-5">
                            <span>
                                <span className="text-sm flex gap-2">
                                    <CiCircleCheck size="20px" />   {' '} {Popmsg}
                                </span>

                            </span>
                        </Alert>
                    )}
                    <div className="overflow-x-auto relative shadow-md ">
                        <div className="flex justify-start md:justify-end items-center bg-white px-4 py-4 gap-5 flex-wrap md:flexnowrap">
                            <div className="dark:bg-gray-900">
                                <label htmlFor="table-search" className="sr-only">Search</label>
                                <div className="relative mt-1">
                                    <div className="flex absolute inset-y-0 left-0 items-center pl-3 pointer-events-none">
                                        <svg className="w-5 h-5 text-gray dark:text-gray" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" clipRule="evenodd"></path></svg>
                                    </div>
                                    <input onChange={handleChange} value={search} type="text" id="table-search" className="block focus:ring-gray-500 focus:border-gray-500 p-2 pl-10 w-80 text-sm text-gray-900 bg-gray-50 border border-gray  dark:bg-gray-700 dark:border-gray dark:placeholder-gray dark:text-white " placeholder="Search for items" />
                                </div>
                            </div>
                            <div className="flex gap-2">
                                <button onClick={onFilter} className="text-sm flex gap-2 bg-blue items-center border border-bg-blue px-2 py-2 text-white">Search</button>
                                <button onClick={onReset} className="text-sm flex gap-2 bg-white items-center border border-bg-red px-3 text-red">Reset</button>
                            </div>
                        </div>
                        <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                            <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                                <tr>
                                    <th scope="col" className="py-3 px-6">
                                        Image
                                    </th>
                                    <th scope="col" className="py-3 px-6">
                                        Name
                                    </th>
                                    <th scope="col" className="py-3 px-6">
                                        Price
                                    </th>
                                    <th scope="col" className="py-3 px-6">
                                        Discount
                                    </th>
                                    <th scope="col" className="py-3 px-6">
                                        Stock
                                    </th>
                                    <th scope="col" className="py-3 px-6">
                                        Category
                                    </th>
                                    <th scope="col" className="py-3 px-6">
                                        Created by
                                    </th>
                                    <th scope="col" className="py-3 px-6 text-center">
                                        Created at
                                    </th>
                                    <th scope="col" className="py-3 px-6 text-center">
                                        Action
                                    </th>
                                </tr>
                            </thead>

                            <tbody>
                                {displayData.map((prod, index) => (

                                    <tr key={index} className="bg-white border-b dark:bg-gray-900 border-gray-light hover:bg-gray-light">

                                        <th scope="row" className="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                            {loading == true ? (
                                                <Skeleton />
                                            ) : (<img src={prod.image_url} width="100" className="object-cover w-40 " />
                                            )}
                                        </th>
                                        <td className="py-4 px-6">
                                            {loading == true ? (
                                                <Skeleton />
                                            ) : (
                                                <span>
                                                    {prod.nama}
                                                </span>
                                            )}
                                        </td>
                                        <td className="py-4 px-6">
                                            {loading == true ? (
                                                <Skeleton />
                                            ) : (
                                                <span>
                                                    Rp. {prod.harga_display}
                                                </span>
                                            )}
                                        </td>
                                        <td className="py-4 px-6">
                                            {loading == true ? (
                                                <Skeleton />
                                            ) : (
                                                <div>
                                                    {prod.is_diskon === 1 && <span> Rp. {prod.harga_diskon_display}</span>}
                                                    {prod.is_diskon === 0 && <span>-</span>}
                                                </div>
                                            )}
                                        </td>
                                        <td className="py-4 px-6">
                                            {loading == true ? (
                                                <Skeleton />
                                            ) : (
                                                <span> {prod.stock} </span>
                                            )}
                                        </td>
                                        <td className="py-4 px-6">
                                            {loading == true ? (
                                                <Skeleton />
                                            ) : (
                                                <span className="bg-red text-white px-2 py-2 capitalize">{prod.category}</span>
                                            )}
                                        </td>
                                        <td className="py-4 px-6">
                                            {loading == true ? (
                                                <Skeleton />
                                            ) : (
                                                <span>{prod.user.name}</span>
                                            )}
                                        </td>
                                        <td className="py-4 px-6">
                                            {loading == true ? (
                                                <Skeleton />
                                            ) : (
                                                <span> {prod.created_at}</span>
                                            )}
                                        </td>
                                        <td className="py-4 px-6">
                                            {loading == true ? (
                                                <Skeleton />
                                            ) : (
                                                <div className="flex gap-3">
                                                    <button className="underline text-red" onClick={() => handleModalOpen(prod.id)}><b>Delete</b></button>
                                                    <Link to={`/setting/edit/${prod.id}`}><button className="underline text-blue"><b>Edit</b></button></Link>
                                                    
                                                </div>
                                            )}
                                        </td>

                                    </tr>

                                ))}


                            </tbody>

                        </table>
                    </div>

                </div>
            </div>

            <Modal className="bg-gray"
                show={openModal}
                size="md"
                popup={true}
                onClose={() => setOpenModal(false)}
            >
                <Modal.Header />
                <Modal.Body>
                    <div className="text-center">
                        <h3 className="mb-5 text-base text-gray-500 dark:text-gray-400">
                            Delete this product?
                        </h3>
                        <div className="flex justify-center gap-4 ">
                            <Button className="bg-red rounded-sm"
                                color="failure"
                                onClick={() => deleteProduct(deleteBtn)}
                            >
                                Delete
                            </Button>
                            <Button className="border-red rounded-sm"
                                color="gray"
                                onClick={() => setOpenModal(false)}
                            >
                                Cancel
                            </Button>
                        </div>
                    </div>
                </Modal.Body>
            </Modal>

        </div >

    );
}

export default TableAllProduct;

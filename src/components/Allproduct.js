import React, { useEffect, useContext } from "react";
import { ProdContext } from "../GlobalContext";
import { Link } from "react-router-dom";
import Skeleton from 'react-loading-skeleton'

function Allproduct() {
  const { getProduct, loading, displayData} = useContext(ProdContext);

  const MAX_LENGTH = 45;
  useEffect(() => {
    getProduct();
  }, []);

  return (
    <div className="bg-gray-light pt-[100px]">
     
      <div className="container mx-auto ">
        <div className="mt-5">
          <div className="flex flex-wrap gap-x-2">
            {displayData.map((prod) => (
              <div className="bg-white w-full max-w-[190px] shadow-md hover:shadow-lg cursor-pointer">
                <Link to={`/product/${prod.id}`}>
                  {loading == true ? (
                    <Skeleton />
                  ) : (
                    <img className="transition duration-150 ease-in-out text-center mx-auto object-cover h-48 w-96" src={prod.image_url} />
                  )}
                  <div className="px-4 pb-2 flex flex-col h-40">
                    {loading == true ? (
                      <Skeleton />
                    ) : (
                      <div className="mt-4 mb-3">
                        {prod.nama.length > MAX_LENGTH ?
                          (
                            <div>
                              <p className="text-left text-sm">{`${prod.nama.substring(0, MAX_LENGTH)}...`}</p>
                            </div>
                          ) :
                          <p className="text-left text-sm">{prod.nama}</p>
                        }
                      </div>
                    )}

                    {loading == true ? (
                      <Skeleton />
                    ) : (
                      <div className="mt-auto">
                        {prod.is_diskon === 1 && (
                          <div>
                            <p className="text-red"><b>Rp. <span>{prod.harga_diskon_display}</span></b></p>
                            <p className="text-gray"><strike>Rp. <span>{prod.harga_display}</span></strike></p>
                          </div>
                        )}
                        {prod.is_diskon === 0 && (
                          <div>
                            <p className=""><b>Rp. <span>{prod.harga_display}</span></b></p>
                          </div>
                        )}

                        <p className="text-sm text-gray mt-3">stock : <span>{prod.stock}</span> </p>
                      </div>
                    )}
                  </div>
                </Link>
              </div>

            ))}
          </div>

        </div>
      </div>

    </div>
  );
}

export default Allproduct;

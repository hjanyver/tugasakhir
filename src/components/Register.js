import axios from "axios";
import React, { useEffect, useState, useContext } from "react";
import { Link, useNavigate } from "react-router-dom";
import { CiCircleCheck, CiWarning } from "react-icons/ci";
import { ProdContext } from "../GlobalContext";
import { Formik } from "formik";
import { Alert } from 'flowbite-react';
import * as Yup from 'yup';

const Register = () => {

    //navigation
    const navigate = useNavigate();
    const getuser = localStorage.getItem('username');

    //validation message
    const validationScheme = Yup.object().shape({
        name: Yup.string().required("Name is required"),
        username: Yup.string().required("Username is required"),
        email: Yup.string().email('Invalid email address').required('Email address is required'),
        password: Yup.string().required("Password is required"),
        password_confirmation: Yup.string()
            .oneOf([Yup.ref('password'), null], 'Passwords must match').required("Confirmation password is required")

    });

    const { input, setInput, ShowMessage, setShowMessage, Poperror, setPoperror, Popmsg } = useContext(ProdContext);


    //handle formik 
    const FormState = {
        email: "",
        password: "",
        name: "",
        username: "",
        password_confirmation: ""
    }

    //HandleSubmit
    const handleSubmit = async (values) => {

        console.log(values);
        try {
            const response = await axios.post(
                "https://arhandev.xyz/public/api/final/register",
                {
                    email: values.email,
                    name: values.name,
                    username: values.username,
                    password: values.password,
                    password_confirmation: values.password_confirmation
                }
            );

            //show message success
            setPoperror(response.data.info);
            setShowMessage(true);

            setInput({
                email: "",
                password: "",
                name: "",
                username: "",
                password_confirmation: ""

            });

            navigate('/login');
        } catch (error) {
            //show error
            console.log(error);
            setPoperror(error.response.data.info);
            setShowMessage(true);
        }
    };

    useEffect(() => {
        //check if logged
        if (getuser !== null) {
            navigate('/');
        };
    });

    return (
        <div className="grid place-items-center h-screen ">
            <div className="container mx-auto px-10">

                <div className="bg-white mt-4 px-10 py-10 mx-auto shadow-lg sm:rounded-lg  md:w-1/2">
                    <p className="text-md mb-4"><b>REGISTER</b></p>
                    {ShowMessage === true && (
                        <Alert className="bg-light-green mb-5">
                            <span>
                                <span className="text-sm flex gap-2">
                                    <CiWarning size="20px" />   {' '} {Poperror}
                                </span>

                            </span>
                        </Alert>
                    )}
                    <Formik onSubmit={handleSubmit} initialValues={FormState} validationSchema={validationScheme}>
                        {params => {
                            return (
                                <form>
                                    <div className="mb-4">
                                        <label htmlFor="name" className="block mb-2 text-normal">Name</label>
                                        <input value={params.values.name}
                                            onChange={params.handleChange}
                                            onBlur={params.handleBlur}
                                            type="text" id="name" className="border border-gray hover:border-light-green text-grey text-sm rounded-lg focus:outline-none focus:border-1 focus:ring-1 focus:ring-light-green block w-full p-2.5" placeholder="Enter your name" />
                                        {params.touched.name && params.errors.name && <label className="text-sm text-red">{params.errors.name}</label>}
                                    </div>
                                    <div className="mb-4">
                                        <label htmlFor="username" className="block mb-2 text-normal">Username</label>
                                        <input value={params.values.username}
                                            onChange={params.handleChange}
                                            onBlur={params.handleBlur}
                                            type="text" id="username" className="border border-gray hover:border-light-green text-grey text-sm rounded-lg focus:outline-none focus:border-1 focus:ring-1 focus:ring-light-green block w-full p-2.5" placeholder="Enter your username" />
                                        {params.touched.username && params.errors.username && <label className="text-sm text-red">{params.errors.username}</label>}
                                    </div>
                                    <div className="mb-4">
                                        <label htmlFor="email" className="block mb-2 text-normal">Email</label>
                                        <input value={params.values.email}
                                            onChange={params.handleChange}
                                            onBlur={params.handleBlur}
                                            type="text" id="email" className="border border-gray hover:border-light-green text-grey text-sm rounded-lg focus:outline-none focus:border-1 focus:ring-1 focus:ring-light-green block w-full p-2.5" placeholder="Email address" />
                                        {params.touched.email && params.errors.email && <label className="text-sm text-red">{params.errors.email}</label>}
                                    </div>
                                    <div className="mb-4">
                                        <label htmlFor="password" className="block mb-2 text-normal">Password</label>
                                        <input value={params.values.password}
                                            onChange={params.handleChange}
                                            onBlur={params.handleBlur}
                                            type="password" id="password" className="border border-gray hover:border-light-green text-grey text-sm rounded-lg focus:outline-none focus:border-1 focus:ring-1 focus:ring-light-green block w-full p-2.5" placeholder="Password" />
                                        {params.touched.password && params.errors.password && <label className="text-sm text-red">{params.errors.password}</label>}
                                    </div>
                                    <div className="mb-4">
                                        <label htmlFor="password_confirmation" className="block mb-2 text-normal">Password confirmation</label>
                                        <input value={params.values.password_confirmation}
                                            onChange={params.handleChange}
                                            onBlur={params.handleBlur}
                                            type="password" id="password_confirmation" className="border border-gray hover:border-light-green text-grey text-sm rounded-lg focus:outline-none focus:border-1 focus:ring-1 focus:ring-light-green block w-full p-2.5" placeholder="Password" />
                                        {params.touched.password_confirmation && params.errors.password_confirmation && <label className="text-sm text-red">{params.errors.password_confirmation}</label>}
                                    </div>
                                    <div className="mb-10">
                                        <button onClick={params.handleSubmit}
                                            type="submit"
                                            name="submit" className={params.dirty && params.isValid ? 'text-normal drop-shadow-xl border rounded text-white bg-blue px-4 py-2 flex items-center enabled' : 'text-normal drop-shadow-xl border  rounded text-white bg-blue px-4 py-2 flex items-center opacity-75'} ><CiCircleCheck size="22px" className="pr-1" />Register now</button>
                                    </div>
                                    <p className="text-sm"><Link to={"/login"}><span className="text-blue-500 underline">LOGIN</span></Link> </p>
                                </form>
                            )
                        }}
                    </Formik>
                </div>
            </div>
        </div>
    );
};

export default Register;
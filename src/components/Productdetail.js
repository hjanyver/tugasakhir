import React, { useEffect, useState, useContext } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { ProdContext } from "../GlobalContext";
import { Link } from "react-router-dom";

import Skeleton, { SkeletonTheme } from 'react-loading-skeleton'

function Productdetail() {

    const { id } = useParams();
    const { getProduct, getProductDetail, Prods, loading, Fedit, setFedit, setLoading, EsetInput, Einput, setInput, input } = useContext(ProdContext);
    const MAX_LENGTH = 45;
    useEffect(() => {
        getProductDetail(id);
        getProduct();
    }, []);

    return (
        <div className="bg-gray-light pt-[150px]">


            {/* Product detail */}
            <div className="container mx-auto ">
                <div className="mt-5">


                    <div className="px-5 py-5 bg-white flex flex-wrap md:flex-nowrap gap-10">
                        <div className="">
                            {loading == true ? (
                                <Skeleton />
                            ) : (
                                <img className="transition duration-150 ease-in-out text-center mx-auto object-cover  w-full max-w-xl" src={Einput.image_url} />
                            )}
                        </div>
                        <div className="w-full">
                            <p className="mb-6 text-xl"><b>{Einput.nama}</b></p>
                            <div className="bg-gray-light mb-10 px-5 py-5">


                                {Einput.is_diskon2 === 1 && (
                                    <div className="flex gap-4 items-center">
                                        <p className="text-gray text-normal"><strike>Rp. <span>{Einput.harga_display}</span></strike></p>
                                        <p className="text-red text-xl"><b>Rp. <span>{Einput.harga_diskon_display}</span></b></p>
                                    </div>
                                )}
                                {Einput.is_diskon2 === 0 && (
                                    <div>
                                        <p className="text-lg"><b>Rp. <span>{Einput.harga_display}</span></b></p>
                                    </div>
                                )}
                            </div>

                            <div className="flex gap-10 mb-4">
                                <p className="text-gray text-normal w-32 max-w-[100px]">Category</p>
                                <p className=" text-normal w-full text-left">{Einput.category}</p>
                            </div>
                            <div className="flex gap-10 mb-4">
                                <p className="text-gray text-normal w-32 max-w-[100px]">Stock</p>
                                <p className=" text-normal w-full text-left">{Einput.stock}</p>
                            </div>
                            <div className="flex gap-10 ">
                                <p className="text-gray text-normal w-32 max-w-[100px]">Description</p>
                                <p className=" text-normal w-full text-left">{Einput.description}</p>
                            </div>

                        </div>
                    </div>


                    <div className="pt-10 mb-4">
                        <p><b>Another products you may like</b></p>
                    </div>
                    <div className="flex flex-wrap gap-x-2 ">

                        {Prods.map((prod) => (
                            <Link target={"_blank"} to={`/product/${prod.id}`}>
                                <div className="bg-white w-full max-w-[190px] shadow-md hover:shadow-lg cursor-pointer">
                                    {loading == true ? (
                                        <Skeleton />
                                    ) : (
                                        <img className="transition duration-150 ease-in-out text-center mx-auto object-cover h-48 w-96" src={prod.image_url} />
                                    )}
                                    <div className="px-4 pb-2 flex flex-col h-40">
                                        {loading == true ? (
                                            <Skeleton />
                                        ) : (
                                            <div className="mt-4 mb-3">
                                                {prod.nama.length > MAX_LENGTH ?
                                                    (
                                                        <div>
                                                            <p className="text-left text-sm">{`${prod.nama.substring(0, MAX_LENGTH)}...`}</p>
                                                        </div>
                                                    ) :
                                                    <p className="text-left text-sm">{prod.nama}</p>
                                                }
                                            </div>
                                        )}

                                        {loading == true ? (
                                            <Skeleton />
                                        ) : (
                                            <div className="mt-auto">
                                                {prod.is_diskon === 1 && (
                                                    <div>
                                                        <p className="text-red"><b>Rp. <span>{prod.harga_diskon_display}</span></b></p>
                                                        <p className="text-gray"><strike>Rp. <span>{prod.harga_display}</span></strike></p>
                                                    </div>
                                                )}
                                                {prod.is_diskon === 0 && (
                                                    <div>
                                                        <p><b>Rp. <span>{prod.harga_display}</span></b></p>
                                                    </div>
                                                )}

                                                <p className="text-sm text-gray mt-3">stock : <span>{prod.stock}</span> </p>
                                            </div>
                                        )}
                                    </div>

                                </div>
                            </Link>
                        ))}

                    </div>

                </div>
            </div>

        </div>
    );
}

export default Productdetail;

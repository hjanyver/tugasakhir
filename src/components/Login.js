import axios from "axios";
import React, { useEffect, useContext } from "react";
import { Link, useNavigate } from "react-router-dom";
import { CiWarning, CiLogin } from "react-icons/ci";
import { ProdContext } from "../GlobalContext";
import { Formik } from "formik";
import { Alert } from 'flowbite-react';
import * as Yup from 'yup';

const Login = () => {
    const navigate = useNavigate();
    const validationScheme = Yup.object().shape({
        email: Yup.string().email('Invalid email address').required('Email address is required'),
        password: Yup.string().required("Password is required"),

    });

    const { input, setInput, ShowMessage, setShowMessage, Poperror, setPoperror, Popmsg } = useContext(ProdContext);
    const getuser = localStorage.getItem('username');
    const FormState = {
        email: '',
        password: ''
    }

    //HandleSubmit
    const handleSubmit = async (values) => {

        try {
            const response = await axios.post(
                "https://arhandev.xyz/public/api/final/login",
                {
                    email: values.email,
                    password: values.password,
                }
            );
            localStorage.setItem('token', response.data.data.token);
            localStorage.setItem('username', response.data.data.user.username);
            setPoperror(response.data.info);
            setShowMessage(true);

            setInput({
                email: "",
                password: "",

            });

            navigate('/');
        } catch (error) {
            console.log(error);
            setPoperror(error.response.data.info);
            setShowMessage(true);
        }
    };
    useEffect(() => {
        if (getuser !== null) {
            navigate('/');
        };
    });
    return (
        <div className="grid place-items-center h-screen">
            <div className="container mx-auto px-10">

                <div className="bg-white mt-4 px-10 py-10 mx-auto shadow-lg sm:rounded-lg  md:w-1/2">
                    <p className="text-md mb-10"><b>LOGIN</b></p>
                    
                    {ShowMessage === true && (
                        <Alert className="mb-5">
                            <span>
                                <span className="text-sm flex gap-2">
                                    <CiWarning size="20px" />   {' '} {Poperror}
                                </span>

                            </span>
                        </Alert>
                    )}
                    <Formik onSubmit={handleSubmit} initialValues={FormState} validationSchema={validationScheme}>
                        {params => {
                            return (
                                <form>
                                    <div className="mb-4">
                                        <label htmlFor="email" className="block mb-2 text-normal">Email</label>
                                        <input value={params.values.email}
                                            onChange={params.handleChange}
                                            onBlur={params.handleBlur}
                                            type="text" id="email" className="border border-gray text-grey text-sm focus:outline-none focus:border-1 focus:ring-1 block w-full p-2.5" placeholder="Email address" />
                                        {params.touched.email && params.errors.email && <label className="text-sm text-red">{params.errors.email}</label>}
                                    </div>
                                    <div className="mb-4">
                                        <label htmlFor="password" className="block mb-2 text-normal">Password</label>
                                        <input value={params.values.password}
                                            onChange={params.handleChange}
                                            onBlur={params.handleBlur}
                                            type="password" id="password" className="border border-gray text-grey text-sm focus:outline-none focus:border-1 focus:ring-1 block w-full p-2.5" placeholder="Password" />
                                        {params.touched.password && params.errors.password && <label className="text-sm text-red">{params.errors.password}</label>}
                                    </div>
                                    <div className="mb-10">
                                        <button onClick={params.handleSubmit}
                                            type="submit"
                                            name="submit" className={params.dirty && params.isValid ? 'text-normal drop-shadow-xl border text-white bg-blue px-4 py-2 flex items-center enabled' : 'text-normal drop-shadow-xl border text-white bg-blue px-4 py-2 flex items-center opacity-75'} ><CiLogin size="22px" className="pr-1" />Login</button>
                                    </div>
                                    <p className="text-sm"><Link to={"/"}><span className="text-dark-green underline">HOME</span></Link> </p>
                                </form>
                            )
                        }}
                    </Formik>
                </div>
            </div>
        </div>
    );
};

export default Login;
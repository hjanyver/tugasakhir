
//This Context for Global State can reuse
import axios from "axios";
import React, { useEffect, useState, createContext, useContext } from "react";
export const ProdContext = createContext();

export const GlobalContext = ({ children }) => {
  const FormState = {
    nama: '',
    harga: '',
    harga_diskon: '',
    is_diskon: false,
    stock: '',
    category: '',
    image_url: '',
    description: ''
  }

  const [Prods, setProd] = useState([]);
  const [displayData, setDisplayData] = useState([]);
  const [Edit, setEdit] = useState({});
  const [Fedit, setFedit] = useState(false);
  const [Popmsg, setmsg] = useState();
  const [ShowMessage, setShowMessage] = useState(false);
  const [loading, setLoading] = useState(false);
  const [Poperror, setPoperror] = useState();
  const [Popsuccess, setPopsuccess] = useState();
  const [ShowMessageSuccess, setShowMessageSuccess] = useState(false);
  const [search, setSearch] = useState('');
  const [filterHighlight, setFilterHighlight] = useState('');
  const [Einput, EsetInput] = useState(FormState);
  const [isdiscount, setIsdiscount] = useState(false);


  const [input, setInput] = useState({
    nama: "",
    harga: "",
    harga_diskon: "",
    is_diskon: "",
    stock: "",
    category: "",
    image_url: "",
    description: ""
  });

  const getProduct = async () => {
    setLoading(true);
    try {
      const response = await axios.get("https://arhandev.xyz/public/api/final/products");

      setProd(response.data.data);
      setDisplayData(response.data.data);
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  };

  const getProductDetail = async (id) => {

    try {
      const response = await axios.get(`https://arhandev.xyz/public/api/products/${id}`);
      EsetInput({
        nama: response.data.data.nama,
        harga: response.data.data.harga,
        stock: response.data.data.stock,
        category: response.data.data.category,
        harga_diskon: response.data.data.harga_diskon,
        image_url: response.data.data.image_url,
        description: response.data.data.description,
        is_diskon2: response.data.data.is_diskon,
        harga_diskon_display: response.data.data.harga_diskon_display,
        harga_display: response.data.data.harga_display
      });
      if (response.data.data.is_diskon === 0) {
        setIsdiscount(false);
      }
      if (response.data.data.is_diskon === 1) {
        setIsdiscount(true);
      }
    } catch (error) {
      console.log(error);
    }

  };

  return (
    <ProdContext.Provider
      value={{
        getProduct: getProduct,
        getProductDetail: getProductDetail,
        input: input,
        setInput: setInput,
        Prods: Prods,
        setProd: setProd,
        Edit: Edit,
        loading: loading,
        setLoading: setLoading,
        setEdit: setEdit,
        search: search,
        setSearch: setSearch,
        filterHighlight: filterHighlight,
        setFilterHighlight: setFilterHighlight,
        Popmsg: Popmsg,
        displayData: displayData,
        setDisplayData: setDisplayData,
        setmsg: setmsg,
        ShowMessage: ShowMessage,
        setShowMessage: setShowMessage,
        Poperror: Poperror,
        setPoperror: setPoperror,
        Popsuccess: Popsuccess,
        setPopsuccess: setPopsuccess,
        ShowMessageSuccess: ShowMessageSuccess,
        setShowMessageSuccess: setShowMessageSuccess,
        Fedit: Fedit,
        Einput: Einput,
        EsetInput: EsetInput,
        setFedit: setFedit,
        isdiscount: isdiscount,
        setIsdiscount: setIsdiscount
      }}
    >
      {children}
    </ProdContext.Provider>
  );
};
